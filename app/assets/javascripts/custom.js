// Generated by CoffeeScript 1.9.1
var ready;

ready = function() {
  if (form_hidden) {
    $('.dream-form').hide();
  } else {
    $('.dream-form').show();
  }
  return $('#add-dream').on("click", function(e) {
    e.preventDefault();
    return $('.dream-form').slideToggle(700, 'easeInSine', function() {});
  });
};

$(document).ready(ready);

$(document).on('page:load', ready);
